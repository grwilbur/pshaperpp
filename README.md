# Pshaper

Pshaper is a program designed to perform time and power-dependent quantum simulations of optically-excited semiconductor quantum dots. This program is distributed under the GPL 3 license. 


## Purpose
Pshaperpp is a program to simulate time and power dependent dynamics of opticallly excited semiconductor quantum dots. Semiconductor quantum dots are a pomising platoform for quantum emitters which are curcial to the emerging fields of quantum computation and quantum cryptography. Due to quantum confinement, semiconductor quantum dots have several, discrete energy levels. Electrons can be excited into one of these discrete levels using a pulse of laser light and when the electron relaxes, a single photon will be emitted. Unfortunaley, the interactions of a coherent source with a multi-level quantum system are not simple; hence this program. 

## Functionality
Multiple exotic pulse shapes can be used to excite quantum dots. Unchriped, linearly chriped, and bichromatic pulses are supported out-of-the box for techniques like adiabatic rapid passage (ARP) and rabi rotation (RO) simulations. Arbitrary phase masks can be applied to pulses in order to adequately chirp pulses. It is quite simple for users to add custom pulse shapes to simulate.


## Project Structure
- _src_ source files, including unit test sources, and headers private to pshaperpp
- _include_ public header files
- _tests_ non unit tests, such as regression tests, or tests of the final application
- _docs_ documentation and supporting information
- _examples_ code examples of pshaperpp

## Running
### Installing Dependencies
Dependencies are managed with [conan](https://conan.io/). As such, installing conan is prerequisite to using this project.
To install dependencies listed in *conanfile.txt*, starting from the root of this project, one must:\
`mkdir build`\
`conan install . -if build --build=missing`\
With this command, the dependencies listed in the conanfile located in the current directory *(.)* will be installed
with references into the install folder *(if)* named build. If the [conan-center](https://conan.io/center/), the default
remote registry, doesn't contain pre-built binaries for your configuration, they will be built locally, allowing conan
to use these builds system-wide.


### Building The Project
#### Generating Build Files with CMake
As this project is not yet a conan package itself, one must build from source, generating the build files with [CMake](https://cmake.org/), a program that defines C/C++ project structure and generates build system files for you.
In the project root (*pshaperpp*, not *pshaperpp/build*), generate build files in the build directory using any build system of your choosing.

Ex:\
_Unix Makefiles_\
`cmake . -B build -DCMAKE_BUILD_TYPE=Release -G 'Unix Makefiles'`

_Ninja Build_\
`cmake . -B build -DCMAKE_BUILD_TYPE=Release -GNinja`

The option *CMAKE_BUILD_TYPE* can be instead set to *Debug* to generate debug binaries from the build system.

#### Executing Build
Once the build files are generated with CMake, the code can be built with whatever build system you chose. \
Ex (executed from project root):
_Unix Makefiles_\
`make -C build`

_Ninja Build_\
`ninja -C build`\
And, binary files will be placed in *pshaperpp/build/bin*

Of course, though, IDEs can automate this, and will often be able to load CMake targets directly.

### TODO
- all headers were put as public headers. Separate these appropriately
- define template definitions in headers, not source
- place all pshaperpp specific code in namespace `pshaperpp` or similar
- export CMake targets
- make this a conan package with conanfile.py instead of just a consumer with conanfile.txt
