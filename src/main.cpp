#include <exception>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <memory>
#include <string>

#include <toml.hpp>
#include <toml/exception.hpp>
#include <toml/parser.hpp>
#include <toml/value.hpp>

#include "qsspp/bloch.h"
#include "qsspp/mask.h"
#include "qsspp/pulses.h"
#include "qsspp/dot.h"




int main(int argc, char ** argv) {
  auto config_file = toml::parse("config/config.toml");

  // Mama mia get ready for spaghetti!

  auto pl = config_file.at("pulse");
  std::unordered_map<std::string, std::function<std::unique_ptr<pulse::pulse>()>>
    pulse_map = 
    {{"gaussian", [pl]() {
        return std::make_unique<pulse::gaussian>
        (1883.65156730885327734099200210159654134311754176209/pl.at("centre_wvl").as_floating(),
         pl.at("phase").as_floating(),
         pl.at("duration").as_floating(),
         pl.at("E0").as_floating(),
         pl.at("polarization").as_string().str.at(0));
    }},
    {"lorentzian", [pl]() {
        return std::make_unique<pulse::lorentzian>
        (1883.65156730885327734099200210159654134311754176209/pl.at("centre_wvl").as_floating(),
         pl.at("phase").as_floating(),
         pl.at("duration").as_floating(),
         pl.at("E0").as_floating(),
         pl.at("polarization").as_string().str.at(0));
    }},
    {"sech", [pl]() {
        return std::make_unique<pulse::sech>
        (1883.65156730885327734099200210159654134311754176209/pl.at("centre_wvl").as_floating(),
         pl.at("phase").as_floating(),
         pl.at("duration").as_floating(),
         pl.at("E0").as_floating(),
         pl.at("polarization").as_string().str.at(0));
    }}
  };
  auto pulse = pulse_map.at(pl.at("shape").as_string())();
  /*
  auto t = 0.;
  (argc == 2 ? t = std::atof(argv[1]): t = 0);
  auto data = pulse->fieldIntensityAtTime(t);
  std::cout << pulse->polarization.x << pulse->polarization.y << "\n";
  std::cout << data << '\n';
  */




  std::unordered_map<std::string, std::function<std::unique_ptr<mask::mask>()>>
    phase_mask_map_with_unique =
    {{"cosine", [config_file]() {
        return std::make_unique<mask::cosine>
        (config_file.at("mask").at("phase").at("cosine").at("alpha").as_floating(),
         config_file.at("mask").at("phase").at("cosine").at("gamma").as_floating(),
         1883.65156730885327734099200210159654134311754176209/config_file.at("mask").at("phase").at("cosine").at("centre_wvl").as_floating(),
         config_file.at("mask").at("phase").at("cosine").at("delta").as_floating());
    }}
  };
  auto phase_mask = phase_mask_map_with_unique.at(config_file.at("mask").at("phase_type").as_string())();






  std::unordered_map<std::string, std::function<std::unique_ptr<mask::mask>()>>
    amp_mask_map_with_unique =
    {{"bichromatic",[config_file]() {
        return std::make_unique<mask::bichromatic>
        (config_file.at("mask").at("amp").at("bichromatic").at("FWHM").as_floating(),
         1883.65156730885327734099200210159654134311754176209/config_file.at("mask").at("amp").at("bichromatic").at("centre_wvl").as_floating());
    }},
    {"none", [config_file]() {
      return std::make_unique<mask::none>();
    }}
  };


  auto amp_mask  = amp_mask_map_with_unique.at(config_file.at("mask").at("amp_type").as_string())();


  dot::dot quantum_dot(config_file.at("dot").at("dot1"));

  auto is = config_file.at("states").at("initial");
  bloch::state initial;

  auto sqr = [](double x) -> double {return x*x;};
  /*
  ρ = |ρ_00   p_0x   ρ_0y   ρ_0b|    |00   01   02   03|    |0      1      2      3   |
      |ρ_x0   ρ_xx   ρ_xy   ρ_xb| =\ |10   11   12   13| =\ |4      5      6      7   |
      |ρ_y0   ρ_yx   ρ_yy   ρ_yb| =/ |20   21   22   23| =/ |8      9      10     11  |
      |ρ_b0   ρ_bx   ρ_by   ρ_bb|    |30   31   32   33|    |12     13     14     15  |
  */
  using namespace std::complex_literals;
  initial[0] = sqr(is.at("c_0r").as_floating());
  initial[1] = is.at("c_0r").as_floating()*is.at("c_xr").as_floating()*std::exp(1i*(is.at("c_0t").as_floating()-is.at("c_xt").as_floating()));
  initial[2] = is.at("c_0r").as_floating()*is.at("c_yr").as_floating()*std::exp(1i*(is.at("c_0t").as_floating()-is.at("c_yt").as_floating()));
  initial[3] = is.at("c_0r").as_floating()*is.at("c_br").as_floating()*std::exp(1i*(is.at("c_0t").as_floating()-is.at("c_bt").as_floating()));
  initial[4] = std::conj(initial[1]);

  initial[5] = sqr(is.at("c_xr").as_floating());
  initial[6] = is.at("c_xr").as_floating()*is.at("c_yr").as_floating()*std::exp(1i*(is.at("c_xt").as_floating()-is.at("c_yt").as_floating()));
  initial[7] = is.at("c_xr").as_floating()*is.at("c_br").as_floating()*std::exp(1i*(is.at("c_xt").as_floating()-is.at("c_bt").as_floating()));
  initial[8] = std::conj(initial[2]);
  initial[9] = std::conj(initial[6]);

  initial[10] = sqr(is.at("c_yr").as_floating());
  initial[11] = is.at("c_yr").as_floating()*is.at("c_br").as_floating()*std::exp(1i*(is.at("c_yt").as_floating()-is.at("c_bt").as_floating()));
  initial[12] = std::conj(initial[3]);
  initial[13] = std::conj(initial[7]);
  initial[14] = std::conj(initial[11]);
  initial[15] = sqr(is.at("c_br").as_floating());


  

  auto int_cfg = config_file.at("integration");
  bloch::solution solution = bloch::solve(initial, quantum_dot, *pulse, 
                                          int_cfg.at("end_time").as_floating(), 
                                          int_cfg.at("time_step").as_floating());

  std::ofstream population("pop.txt");
  for (uint i = 0; i < solution.states.size(); i+=100) {
    population << solution.states[i][0].real() << '\n';
  }
/*
  for (auto s : solution.states)
    population << s[0].real() << '\n';
*/
  population.close();



  return 0;

}