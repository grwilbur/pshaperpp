#include <fftw3.h>
#include <qsspp/fft.h>

// Internal function to actually compute the fft of a set of complex data.
static void fftw_fft(fftw_complex* in, fftw_complex* out, const unsigned N) {
  fftw_plan plan = fftw_plan_dft_1d(N, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(plan);
  fftw_destroy_plan(plan);
  fftw_cleanup();
}

// Computes the complex fourier transform of a given complex vector
std::vector<std::complex<double>> fft::fft(const std::vector<std::complex<double>>& x_v) {
  const size_t N = x_v.size();
  std::vector<std::complex<double>> k(N);

  // caution should be taken with reinterpret cast. Directly casting bytes & const qualifiers is
  // dangerous but is sometimes necessary to interface with filthy C libraries without copying
  // entire contents
  auto* input = reinterpret_cast<fftw_complex*>(const_cast<std::complex<double>*>(x_v.data()));
  auto* output = reinterpret_cast<fftw_complex*>(const_cast<std::complex<double>*>(k.data()));
  fftw_fft(input, output, N);

  return k;
}


std::vector<hlp::cd> fft::ifft(const std::vector<hlp::cd> & x_v) {
  const size_t N = x_v.size();
  std::vector<std::complex<double>> k(N);

  // caution should be taken with reinterpret cast. Directly casting bytes & const qualifiers is
  // dangerous but is sometimes necessary to interface with filthy C libraries without copying
  // entire contents
  auto* input = reinterpret_cast<fftw_complex*>(const_cast<std::complex<double>*>(x_v.data()));
  auto* output = reinterpret_cast<fftw_complex*>(const_cast<std::complex<double>*>(k.data()));
  fftw_fft(input, output, N);

  return k;
}

