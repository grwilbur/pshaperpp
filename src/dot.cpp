#include "../include/qsspp/dot.h"
#include <iostream>


/*
  ρ = |ρ_00   p_0x   ρ_0y   ρ_0b|    |00   01   02   03|    |0      1      2      3   |
      |ρ_x0   ρ_xx   ρ_xy   ρ_xb| =\ |10   11   12   13| =\ |4      5      6      7   |
      |ρ_y0   ρ_yx   ρ_yy   ρ_yb| =/ |20   21   22   23| =/ |8      9      10     11  |
      |ρ_b0   ρ_bx   ρ_by   ρ_bb|    |30   31   32   33|    |12     13     14     15  |
*/

dot::dot::dot(toml::basic_value<toml::discard_comments, std::unordered_map, std::vector> input) {
  omega[1][0] = input["E_0_to_x"].as_floating();
  omega[2][0] = input["E_0_to_y"].as_floating();
  omega[3][0] = input["E_0_to_b"].as_floating();
  omega[2][1] = omega[1][0] - omega[2][0];
  omega[3][1] = omega[3][0] - omega[1][0];
  omega[3][2] = omega[3][0] - omega[2][0];


  dipole[1][0] = input["d_x0"].as_floating();
  dipole[2][0] = input["d_y0"].as_floating();
  dipole[3][0] = input["d_b0"].as_floating();
  dipole[1][2] = input["d_xy"].as_floating();
  dipole[3][1] = input["d_bx"].as_floating();
  dipole[3][2] = input["d_by"].as_floating();
  dipole[0][1] = dipole[1][0];
  dipole[0][2] = dipole[2][0];
  dipole[0][3] = dipole[3][0];
  dipole[2][1] = dipole[1][2];
  dipole[1][3] = dipole[3][1];
  dipole[2][3] = dipole[3][2];


  gamma[0][0] = 1/input["T_oo"].as_floating();
  gamma[1][1] = 1/input["T_xx"].as_floating();
  gamma[2][2] = 1/input["T_yy"].as_floating();
  gamma[3][3] = 1/input["T_bb"].as_floating();
  gamma[1][0] = 1/input["T_xo"].as_floating();
  gamma[2][0] = 1/input["T_yo"].as_floating();
  gamma[3][0] = 1/input["T_bo"].as_floating();
  gamma[1][2] = 1/input["T_xy"].as_floating();
  gamma[3][1] = 1/input["T_bx"].as_floating();
  gamma[3][2] = 1/input["T_by"].as_floating();

  gamma_wl_eid = input["eid_c"].as_floating(); 

}