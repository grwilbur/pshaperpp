#include <qsspp/helper.h>

#include <cmath>
#include <complex>
#include <cstddef>
#include <fstream>
#include <stdexcept>

using namespace std::complex_literals;

// TODO: should numPoints be unsigned or std::size_t?
std::vector<double> hlp::linspace_inclusive(const double start, const double stop,
                                            const unsigned numPoints) {
  std::vector<double> xv(numPoints + 1);
  const double step = (stop - start) / (numPoints - 1);
  for (unsigned pointIdx = 0; pointIdx <= numPoints; ++pointIdx) {
    xv[pointIdx] = start + step * pointIdx;
  }
  return xv;
}

std::vector<double> hlp::linspace_exclusive(const double start, const double stop,
                                            const unsigned numPoints) {
  std::vector<double> xv(numPoints);
  const double step = (stop - start) / (numPoints);
  for (unsigned pointIdx = 0; pointIdx < numPoints; ++pointIdx) {
    xv[pointIdx] = start + step * pointIdx;
  }
  if (start >= 0) {
    xv.pop_back();
  }
  return xv;
}

std::vector<double> hlp::arange(const double start, const double stop, const double step) {
  const auto numPoints = static_cast<unsigned>(std::round((stop - start) / step));
  std::vector<double> xv(numPoints);
  for (unsigned pointIdx = 0; pointIdx < numPoints; ++pointIdx) {
    xv[pointIdx] = start + pointIdx * step;
  }
  return xv;
}

std::vector<double> hlp::forward(const double start, const double step, const unsigned points) {
  std::vector<double> xv(points);
  for (unsigned pointIdx = 0; pointIdx < points; ++pointIdx) {
    xv[pointIdx] = start + step * pointIdx;
  }
  return xv;
}

std::vector<double> hlp::read_text(char* fname) {
  std::ifstream file(fname, std::ios::in);
  std::vector<double> data;
  std::string line;
  while (std::getline(file, line)) {
    data.push_back(std::stod(line));
  }
  file.close();
  return data;
}

double hlp::trapz(const std::vector<double>& fx, const std::vector<double>& x) {
  double sum = 0;
  for (size_t i = 0; i < fx.size() - 1; i++) {
    sum += fx[i] + fx[i + 1];
  }
  sum *= 0.5 * (x[1] - x[0]);
  return sum;
}

hlp::v2d::v2d(char sym): x{0}, y{0} {
  auto ir2 = 1/std::sqrt(2);
  if (sym == 'H') {
    x=0; y=1;
  }
  else if (sym == 'V') {
    x=1; y=0;
  }
  else if (sym == 'R') {
    x=ir2; y = -1i*ir2;
  }
  else if (sym == 'L'){
    x=ir2; y=1i*ir2;
  }
  else {
    throw std::invalid_argument("Polarization must be one of H, V, R, or L");
  }
}


hlp::v2d hlp::v2d::normalize() {
  auto den = std::sqrt(x.real()*x.real() + x.imag()*x.imag() + y.real()*y.real() + y.imag()*y.imag());
  v2d new_one(x/=den,y/=den);

  return new_one;
}

hlp::cd hlp::dot_prod(v2d a,  v2d b) {
  return std::conj(a.x) * b.x + std::conj(a.y)*b.y;
}