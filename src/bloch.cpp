#include "qsspp/bloch.h"
#include "qsspp/fft.h"
#include <boost/numeric/odeint.hpp>
#include "../include/qsspp/pulses.h"
#include "../include/qsspp/dot.h"



bloch::solution bloch::solve(const bloch::state & initial_state, const dot::dot & q, const pulse::pulse & p, const double t_range, const double delta_t) {
  
  // The array of rabi frequecies between the states.
  std::array<std::array< std::complex<double>, 4>, 4> mu;

  // lambda which returns the derivatives determine the states

  // We use a lambda here because we need to modify the system as a function of time 
  // by chaning mu as the pulse arrives at the dot but the function signature 
  // has to be accepted by the boost::stepper
  
  auto bloch = [&mu, q, &p](const bloch::state & s, bloch::state & ds, double time) -> void {
    // Computing upper half of density matrix
    ds[0] = -(1i/2.0)*(mu[0][1]*s[4] - mu[1][0]*s[1] - mu[2][0]*s[2] + mu[0][2]*s[8]) + q.gamma[1][1]*s[5] + q.gamma[2][2]*s[10];

    ds[4] = -(1i/2.0)*(2.0*(q.omega[1][0] - p.omega_0)*s[4] + mu[1][3]*s[12] - mu[2][0]*s[6] + mu[1][0]*(s[0] - s[5])) - (q.gamma[1][0] + q.gamma_wl_eid)*s[4];
    ds[5] = -(1i/2.0)*(mu[1][0]*s[1] - mu[0][1]*s[4] - mu[3][1]*s[7] + mu[1][3]*s[13]) - q.gamma[1][1]*s[5] + (q.gamma[3][3]/2.0)*s[15];

    ds[8] = -(1i/2.0)*(2.0*(q.omega[2][0] - p.omega_0)*s[8] + mu[2][3]*s[12] - mu[1][0]*s[9] + mu[2][0]*(s[0] - s[10])) - (q.gamma[2][0] + q.gamma_wl_eid)*s[8];
    ds[9] = -(1i/2.0)*(2.0*q.omega[2][1]*s[9] + mu[2][0]*s[1] - mu[0][1]*s[8] + mu[2][3]*s[13] - mu[3][1]*s[11]) - (q.gamma[1][2] + q.gamma_wl_eid)*s[6];
    ds[10] = -(1i/2.0)*(mu[2][0]*s[2] - mu[0][2]*s[8] - mu[3][2]*s[11] + mu[2][3]*s[14]) - q.gamma[2][2]*s[10] + (q.gamma[3][3]/2.0)*s[15];

    ds[12] = -(1i/2.0)*(2.0*(q.omega[3][0] - 2.0*p.omega_0)*s[12] + mu[3][1]*s[4] - mu[1][0]*s[13] + mu[3][2]*s[8] - mu[2][0]*s[14]) - (q.gamma[3][0] + q.gamma_wl_eid)*s[12];
    ds[13] = -(1i/2.0)*(2.0*(q.omega[3][1] - p.omega_0)*s[13] + mu[3][2]*s[9] - mu[0][1]*s[12] + mu[3][1]*(s[5] - s[15])) - (q.gamma[3][1] + q.gamma_wl_eid)*s[13];
    ds[14] = -(1i/2.0)*(2.0*(q.omega[3][2] - p.omega_0)*s[14] + mu[3][1]*s[6] - mu[0][2]*s[12] + mu[3][2]*(s[10] - s[15])) - (q.gamma[3][2] + q.gamma_wl_eid)*s[14];
    ds[15] = -(1i/2.0)*(mu[3][1]*s[7] - mu[1][3]*s[13] - mu[2][3]*s[14] + mu[3][2]*s[11]) - q.gamma[3][3]*s[15];

    // Computing lower half of density matrix 
    ds[1] = std::conj(ds[4]);
    ds[2] = std::conj(ds[8]);
    ds[3] = std::conj(ds[12]);

    ds[6] = std::conj(ds[9]);
    ds[7] = std::conj(ds[13]);

    ds[11] = std::conj(ds[14]);

    // for (auto elem : ds)
      // std::cout << elem << '\n';
    // std::cout << '\n';
};


/*
  ρ = |ρ_00   p_0x   ρ_0y   ρ_0b|    |00   01   02   03|    |0      1      2      3   |
      |ρ_x0   ρ_xx   ρ_xy   ρ_xb| =\ |10   11   12   13| =\ |4      5      6      7   |
      |ρ_y0   ρ_yx   ρ_yy   ρ_yb| =/ |20   21   22   23| =/ |8      9      10     11  |
      |ρ_b0   ρ_bx   ρ_by   ρ_bb|    |30   31   32   33|    |12     13     14     15  |
*/
  // Array of rabi frequencies between each state

  // Variable to store the electric field strenght at a given time
  std::complex<double> field;

  // The current system density matrix
  bloch::state current_state = initial_state;

  // Opening a file to write and save the pulse intensity
  std::ofstream f;
  f.open("pulse_time.txt");





  // SOLVER
  bloch::solution sol;
  boost::numeric::odeint::runge_kutta4<bloch::state> stepper;


  for(double t = -t_range; t < t_range; t += delta_t) {
    // Pushing the states and the current time
    sol.states.push_back(current_state);
    sol.time.push_back(t);

    // Determining filed intensity and writing it to the file 
    field = p.fieldStrengthAtTime(t);
    f << field.real() << '\n';
    // std::cout << p.omega_0 << '\n';
    // std::cout << q.omega[1][0] << '\n';
    
    // Determining the rabi frequecies between each state,
    // depends on the pulse
    mu[1][0] = -q.dipole[1][0] * field;//*p.polarization.x;
    mu[2][0] = -q.dipole[2][0] * field;//*p.polarization.y;
    mu[3][0] = -q.dipole[3][0] * field;
    mu[1][2] = -q.dipole[1][2] * field;
    mu[3][1] = -q.dipole[3][1] * field;//*p.polarization.x;
    mu[3][2] = -q.dipole[3][2] * field;//*p.polarization.y;
    mu[0][1] = std::conj(mu[1][0]);
    mu[0][2] = std::conj(mu[2][0]);
    mu[0][3] = std::conj(mu[3][0]);
    mu[2][1] = std::conj(mu[1][2]);
    mu[1][3] = std::conj(mu[3][1]);
    mu[2][3] = std::conj(mu[3][2]);

    // std::cout << mu[1][0] << '\n';
    // std::cout << mu[2][0] << '\n';
    // std::cout << mu[3][0] << '\n';
    // std::cout << mu[1][2] << '\n';
    // std::cout << mu[3][1] << '\n';
    // std::cout << mu[3][2] << '\n';
    // std::cout << mu[0][1] << '\n';
    // std::cout << mu[0][2] << '\n';
    // std::cout << mu[0][3] << '\n';
    // std::cout << mu[2][1] << '\n';
    // std::cout << mu[1][3] << '\n';
    // std::cout << mu[2][3] << "\n\n";

    
    
    // Computing the derivatives at a given step
    stepper.do_step(bloch, current_state, t, delta_t);
  }


  f.close();

  return sol;
}
