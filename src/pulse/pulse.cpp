#include "pulse.h"

#include <numbers>

pulse::pulse::pulse(double omega_0, double phase, double duration,
             double pulse_amplitude, char pol)
    : omega_0{omega_0},
      phase{phase},
      duration{duration},
      pulse_amplitude{pulse_amplitude},
      polarization{pol} {}

