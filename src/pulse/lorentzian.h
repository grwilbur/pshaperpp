#include "pulse.h"



namespace pulse {
// Pulse with lorentzian envelope
class lorentzian : public pulse {
 public:
  // Temporal width prarmeter > 1/(a^2+x^2)
  double temporal_width_param;

  [[nodiscard]] std::complex<double> fieldStrengthAtTime(double t) const noexcept override;
  [[nodiscard]] double fieldIntensityAtTime(double t) const noexcept override;


  [[nodiscard]] std::complex<double> fieldStrengthAtFreq(double omega) const noexcept override;
  [[nodiscard]] double fieldIntensityAtFreq(double omega) const noexcept override;

  lorentzian() = default;
  lorentzian(double, double, double, double, char);
};

}