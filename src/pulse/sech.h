#include "pulse.h"

namespace pulse {

// Secant pulse 
class sech : public pulse {
 public:
  double temporal_width_param;


  std::complex<double> fieldStrengthAtTime(double t) const noexcept override;
               double  fieldIntensityAtTime(double t) const noexcept override;

  std::complex<double> fieldStrengthAtFreq(double omega) const noexcept override;
               double  fieldIntensityAtFreq(double omega) const noexcept override;

  sech() = default;
  sech(double, double, double, double, char);
};


}
