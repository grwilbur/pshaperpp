#include "gaussian.h"
#include <numbers>
#include <iostream>

std::complex<double> pulse::gaussian::fieldStrengthAtTime(const double t) const noexcept {
  return pulse_amplitude * std::exp(- temporal_width_param * t * t -1i*(omega_0 * t + phase));
}

double pulse::gaussian::fieldIntensityAtTime(const double t) const noexcept {
  return pulse_amplitude * pulse_amplitude * std::exp(-2 * temporal_width_param * t * t);
}

std::complex<double> pulse::gaussian::fieldStrengthAtFreq(const double omega) const noexcept {
  return pulse_amplitude * std::sqrt(1. / (std::numbers::pi * temporal_width_param)) *
         std::exp(-(omega - omega_0) * (omega - omega_0) / (4. * temporal_width_param));
}

double pulse::gaussian::fieldIntensityAtFreq(const double omega) const noexcept {
  return 1 / (std::numbers::pi * temporal_width_param) *
             std::exp(-temporal_width_param / 2. * ((omega - omega_0) * (omega - omega_0)) /
             (temporal_width_param * temporal_width_param));
}

pulse::gaussian::gaussian(double omega_0, double phase, double duration, double pulse_amplitude, char pol)
    : pulse::pulse::pulse(omega_0, duration, phase, pulse_amplitude, pol), 
      temporal_width_param{2.77258872223978123766892848583270627230200053744102 / (duration * duration)} 
    {}
