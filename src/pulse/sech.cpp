#include "sech.h"
#include <numbers>
#include <cmath>


using namespace std::complex_literals;

std::complex<double> pulse::sech::fieldStrengthAtTime(const double t) const noexcept {
  return std::exp(-1i*(omega_0 * t + phase))/std::cosh(temporal_width_param*t);
}

double pulse::sech::fieldIntensityAtTime(const double t) const noexcept {
  double den = std::cosh(temporal_width_param*t);
  return pulse_amplitude*pulse_amplitude / den*den;
}

std::complex<double> pulse::sech::fieldStrengthAtFreq(const double omega) const noexcept {
  return pulse_amplitude*
         1.25331413731550025120788264240552262650349337030496/(std::sqrt(temporal_width_param)*
         std::cosh(std::numbers::pi*(omega - omega_0)/(2*temporal_width_param)));
}

double pulse::sech::fieldIntensityAtFreq(const double omega) const noexcept {
  double den = std::cosh(std::numbers::pi*(omega - omega_0)/(2*temporal_width_param));
  return pulse_amplitude*pulse_amplitude*0.125/(temporal_width_param*den*den);
}

pulse::sech::sech(double omega_0, double phase, double full_width_half_max,
                   double pulse_amplitude, char pol)
    : pulse::pulse::pulse(omega_0, full_width_half_max, phase, pulse_amplitude, pol),
      temporal_width_param{2.63391579384963341725009269461593688805396394293503 / duration}
      {}
