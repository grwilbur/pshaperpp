#include "gaussian.h"
#include <numbers>

std::complex<double> pulse::debug::gaussian_DEBUG::fieldStrengthAtTime(const double t) const noexcept {
  return pulse_amplitude * std::exp(- temporal_width_param* t *t -1i*(chirp* t * t + omega_0 * t + phase));
}

double pulse::debug::gaussian_DEBUG::fieldIntensityAtTime(const double t) const noexcept {
  return pulse_amplitude * pulse_amplitude * std::exp(-2 * temporal_width_param * t * t);
}

std::complex<double> pulse::debug::gaussian_DEBUG::fieldStrengthAtFreq(const double omega) const noexcept {
  return pulse_amplitude * std::sqrt(1. / (std::numbers::pi * gaussian_gamma)) *
         std::exp(-(omega - omega_0) * (omega - omega_0) / (4. * gaussian_gamma));
}

double pulse::debug::gaussian_DEBUG::fieldIntensityAtFreq(const double omega) const noexcept {
  return 1 / (std::numbers::pi * std::abs(gaussian_gamma)) *
             std::exp(-temporal_width_param / 2. * ((omega - omega_0) * (omega - omega_0)) /
             (temporal_width_param * temporal_width_param + chirp * chirp));
}

pulse::debug::gaussian_DEBUG::gaussian_DEBUG(double omega_0, double chirp, double phase, double full_width_half_max,
                   double pulse_amplitude)
    : pulse::pulse::pulse(omega_0, full_width_half_max, phase, pulse_amplitude) {
    this->chirp = chirp;
    temporal_width_param = 2.77258872223978123766892848583270627230200053744102 / (duration * duration);
    }
