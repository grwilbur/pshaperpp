#pragma once

#include <complex>
#include "../../include/qsspp/helper.h"
using namespace std::complex_literals;


namespace pulse {

class pulse {
 public:
  // phase information
  const double omega_0{};
  const double phase{};

  // amplitude information
  const double duration{};
  const double pulse_amplitude{};

  const double temporal_width_param{};

  hlp::v2d polarization{};

  virtual std::complex<double> fieldStrengthAtTime(double)  const noexcept = 0;
  virtual              double  fieldIntensityAtTime(double) const noexcept = 0;
  virtual std::complex<double> fieldStrengthAtFreq(double)  const noexcept = 0;
  virtual              double  fieldIntensityAtFreq(double) const noexcept = 0;

  virtual ~pulse() = default;

 protected:
  pulse() = default;
  pulse(double, double, double, double, char);
};










}