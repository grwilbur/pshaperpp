#include "pulse.h"



namespace pulse {
class gaussian : public pulse {
 public:
  // Temporal width prarmeter > e^-width_p*t^2
  double temporal_width_param;

  [[nodiscard]] std::complex<double> fieldStrengthAtTime(double t) const noexcept override;
  [[nodiscard]] double fieldIntensityAtTime(double t) const noexcept override;


  // Spectral width parameter > e^(-(ω-ω0)^2/4Γ)

  [[nodiscard]] std::complex<double> fieldStrengthAtFreq(double omega) const noexcept override;
  [[nodiscard]] double fieldIntensityAtFreq(double omega) const noexcept override;

  gaussian() = default;
  gaussian(double, double, double, double, char);
};



namespace debug {
// Pulse with gaussian envelope with chirp parameter
class gaussian_DEBUG : public pulse {
 public:
  double chirp{};
  // Temporal width prarmeter > e^-width_p*t^2
  double temporal_width_param;

  [[nodiscard]] std::complex<double> fieldStrengthAtTime(double t) const noexcept override;
  [[nodiscard]] double fieldIntensityAtTime(double t) const noexcept override;


  // Spectral width parameter > e^(-(ω-ω0)^2/4Γ)
  const std::complex<double> gaussian_gamma = temporal_width_param - 1i * chirp;

  [[nodiscard]] std::complex<double> fieldStrengthAtFreq(double omega) const noexcept override;
  [[nodiscard]] double fieldIntensityAtFreq(double omega) const noexcept override;

  gaussian_DEBUG() = default;
  gaussian_DEBUG(double, double, double, double, double);
};

}


}




