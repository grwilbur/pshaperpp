#include "lorentzian.h"
#include <numbers>

std::complex<double> pulse::lorentzian::fieldStrengthAtTime(const double t) const noexcept {
  return pulse_amplitude / (temporal_width_param*temporal_width_param +(t*t)) * std::exp(-1i*(omega_0 * t + phase));
}

double pulse::lorentzian::fieldIntensityAtTime(const double t) const noexcept {
  double den = (temporal_width_param*temporal_width_param+t*t);
  return pulse_amplitude*pulse_amplitude / den*den;
}

std::complex<double> pulse::lorentzian::fieldStrengthAtFreq(const double omega) const noexcept {
  return pulse_amplitude * 1.25331413731550025120788264240552262650349337030496/temporal_width_param * 
         std::exp(-temporal_width_param*std::abs(omega- omega_0));
}

double pulse::lorentzian::fieldIntensityAtFreq(const double omega) const noexcept {
  return pulse_amplitude*pulse_amplitude*0.125/(temporal_width_param*temporal_width_param)*
         std::exp(-2*temporal_width_param*std::abs(omega - omega_0));
}

pulse::lorentzian::lorentzian(double omega_0, double phase, double full_width_half_max,
                   double pulse_amplitude, char pol)
    : pulse::pulse::pulse(omega_0, full_width_half_max, phase, pulse_amplitude, pol),
      temporal_width_param{2*std::sqrt((2-duration*duration))}
     {}
