# QSSpp files compiled as static library and linked against executables


set(PULSE_LIB pulse_lib)
add_subdirectory(pulse)

# Gather header and source files
file(GLOB_RECURSE PRIVATE_HDRS LIST_DIRECTORIES false CONFIGURE_DEPENDS *.h)
file(GLOB_RECURSE PUBLIC_HDRS LIST_DIRECTORIES false CONFIGURE_DEPENDS ${PROJECT_SOURCE_DIR}/include/QSSpp/*.h)
set(SRCS bloch.cpp convert.cpp dot.cpp fft.cpp helper.cpp mask.cpp $<TARGET_OBJECTS:pulse_lib>)
set(PULSE_TEST_SRCS pulse/pulse.test.cpp)

# QSSpp static library
set(QSSpp_LIB QSSpp_lib)
add_library(${QSSpp_LIB} STATIC ${SRCS} ${PRIVATE_HDRS} ${PUBLIC_HDRS})
target_include_directories(${QSSpp_LIB}
        PUBLIC ${PROJECT_SOURCE_DIR}/include
        PRIVATE ${PROJECT_SOURCE_DIR}/src)
target_link_libraries(${QSSpp_LIB} PRIVATE CONAN_PKG::fftw CONAN_PKG::boost CONAN_PKG::units)

# QSSpp executable
set(QSSpp_APP QSSpp_app)
add_executable(${QSSpp_APP} ${PROJECT_SOURCE_DIR}/src/main.cpp)
target_link_directories(${QSSpp_APP} PRIVATE ${CMAKE_ARCHIVE_OUTPUT_DIRECTORY})
target_link_libraries(${QSSpp_APP} PRIVATE ${QSSpp_LIB})


# unit test executables
set(PULSE_TEST pulse_unit_test)
add_executable(${PULSE_TEST} ${PULSE_TEST_SRCS})
target_link_libraries(${PULSE_TEST} PRIVATE CONAN_PKG::gtest ${QSSpp_LIB})
target_include_directories(${PULSE_TEST} PRIVATE ${CONAN_INCLUDE_DIRS_FFTW})
add_test(NAME ${PULSE_TEST} COMMAND ${PULSE_TEST})


# IDEs should put the headers in a nice place
source_group(
        TREE "${PROJECT_SOURCE_DIR}/include"
        PREFIX "Public Headers"
        FILES ${HEADER_LIST})

