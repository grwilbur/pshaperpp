#include <qsspp/mask.h>
#include <complex>

// Cosine phasemask mask function defined as:
std::complex<double> mask::cosine::evaluate(const double omega) const noexcept {
  return alpha * std::cos(gamma * (omega - omega_0) - delta);
}



// Bichromatic ampmask function
std::complex<double> mask::bichromatic::evaluate(double omega) const noexcept {
  return (1-std::exp(-width_param*(omega- omega_0)));
}




mask::cosine::cosine(const double alpha, const double gamma, const double omega_0,
                     const double delta)
    : alpha{alpha}, gamma{gamma}, omega_0{omega_0}, delta{delta} {}




mask::bichromatic::bichromatic(const double hole_full_width_half_max, const double omega_0)
    : hole_full_width_half_max{hole_full_width_half_max}, omega_0{omega_0} {}


std::complex<double> mask::none::evaluate(double omega) const noexcept {
  return 1;
}