#pragma once

#include <concepts>
#include <cstddef>

#include "dot.h"
#include "fft.h"
#include "helper.h"
#include "mask.h"
#include "pulses.h"

namespace bloch {

using state = std::array<std::complex<double>, 16>;

struct solution {
  std::vector<state> states;
  std::vector<double> time;
};


// Solve beq like:
// auto data = bloch::twolevel(dot1, pulse, mask, -5, 5, 1000)

solution solve(const state&, const dot::dot&, const pulse::pulse&, const double, const double);



}  // namespace bloch
