#pragma once

#include <fftw3.h>

#include <cmath>
#include <complex>
#include <cstddef>
#include <vector>

#include "helper.h"

namespace fft {
std::vector<double> fftfreq(unsigned, double d = -1);

// Generates the associated frequency bins of a given fourier transform
template <size_t numPoints>
std::array<double, numPoints> freq(double d) {
  if (d == -1) d = 1 / static_cast<double>(numPoints);
  const double halfNumPoints = numPoints / static_cast<double>(2);

  std::vector<double> k1, k2;

  if constexpr ((numPoints bitand 0x01) == 0) {
    const auto uHalfNum = static_cast<unsigned>(halfNumPoints);
    k1 = hlp::forward(0, d, uHalfNum);
    k2 = hlp::forward(-k1.back() - d, d, uHalfNum);
  } else {
    k1 = hlp::forward(0, d, std::ceil(halfNumPoints) + 1);
    k2 = hlp::forward(-k1.back(), d, std::floor(halfNumPoints));
  }

  k1.insert(k1.end(), k2.begin(), k2.end());
  return k1;
}
std::vector<hlp::cd> fft(const std::vector<hlp::cd> &);

std::vector<hlp::cd> ifft(const std::vector<hlp::cd> &);


}  // namespace fft
