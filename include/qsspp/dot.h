#pragma once

#include <map> 
#include "convert.h"
#include "qsspp/helper.h"
#include <toml.hpp>



namespace dot {


//   ^      bi-exciton
// E |    --------------
//   |     /          \
//   |  x /            \
//   | ------        y  \
//   |    \        ---------
//   |     \          /
//   |      \        /
//   |    -------------
//   |       ground


class dot {
  using param_mat = std::array<std::array< double, 4>, 4>;
 public:
  param_mat dipole;
  param_mat omega;
  param_mat gamma;
  double gamma_wl_eid;

  dot() = default;
  dot(std::map<std::string, double>);
  dot(toml::basic_value<toml::discard_comments, std::unordered_map, std::vector>);

};

}  // namespace dot