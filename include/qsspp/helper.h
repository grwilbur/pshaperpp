#pragma once

#include <array>
#include <complex>
#include <vector>

// A set of helper functions that are useful in many places.

// Most are ripped-off from associated numpy functions

namespace hlp {

// Helper typedefs to make life easier when you need some exotic type:

// 2x2 array of complex doubles
using c_22arr = std::array<std::array<std::complex<double>, 2>, 2>;

// nxn vector of complex doubles
using c_2mat = std::vector<std::vector<std::complex<double>>>;

// complex double is used everywhere so it's useful to have an alias
using cd = std::complex<double>;

/*
        Generates a vector of doubles of length N, starting at some start value and ending at some
   end value. Behaves like numpy.linspace in python.

        Example:
        linspace(0, 10, 5)					=> [0, 2.5, 5.0, 7.5, 10]
        linspace(0, 10, 5, endpoint=false) 	=> [0, 2.0, 4.0, 6.0, 8.0]
*/
std::vector<double> linspace_inclusive(double start, double stop, unsigned numPoints);

std::vector<double> linspace_exclusive(double start, double stop, unsigned numPoints);

// Generates a vector of doubles starting at some start values, ending at some
// end value is steps of size d. Behaves like numpy.arange
std::vector<double> arange(double start, double stop, double step);

// Example:
// arange(0, 10, 1)	=> [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
// arange(0, 5, 2)  => [0, 2, 4]

// Generates a vector of doubles of size N, starting at some start value and
// stepping by an amount d. This has no numpy analogue and is mostly used
// when getting fftfreq.
std::vector<double> forward(double, double, unsigned);

// Example:
// forward(1, 3, 6)   => [1, 3, 6, 9, 12, 15]
// forward(0, 0.5, 2) => [0, 0.5]

// Reads data from a file into a std::vector.
std::vector<double> read_text(char *);

// Integrates a vector of doubles using trapezoid rule.
double trapz(const std::vector<double> &, const std::vector<double> &);

// Sech^2(x) function
double sech2(double) noexcept;


// 2D vector that we can use for polarization states of the dot and the dipole
struct v2d {
  cd x, y;
  v2d normalize();

  v2d() :x{0}, y{0}{};
  v2d(cd xi, cd yi) : x{xi}, y{yi} {};
  v2d(char);

};

// Takes the dot product of 2 2d vectors, this might be right but it could also be not right at all

cd dot_prod(v2d, v2d);

}  // namespace hlp
