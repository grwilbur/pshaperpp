#pragma once

#include <numbers>
#include <string>

constexpr double two_pi = 2 * std::numbers::pi;

// physical constants in mksa units
// note: units library (units.h header) has been added as a dependency of this project
const double E_CHARGE = 1.6021766208e-19;     // electron charge (Coulomb)
const double H_BAR = 1.0545718001391127e-34;  // hbar (Joule.second)
const double C = 299792458.0;                 // speed of light (m/s)
const double EPSO = 8.854187817620389e-12;    // permittivity of vacuum (F/m)
const double KB = 1.38064852e-23;             // boltzmann constant (J/K)

const double DEBYE_TO_CM = 1E-21 / C;

// physical constants in ARU units
const double EPSO_ARU = 1 / (4 * std::numbers::pi);
const double H_BAR_ARU = 1.0;
const double E_CHARGE_ARU = 1.0;
const double KB_ARU = 3.166810514923162e-06;
const double C_ARU = 137.03599913838403;

// conversion factors to convert mksa units to atomic rydberg units (aru)
const double ARU_TIME = 2.418884326509e-17;        // (seconds)
const double ARU_FREQUENCY = 4.1341373336493e+16;  // (Hertz)
const double ARU_ELECTRIC_FIELD = 514220670700.0;  // (volts/meter)
const double ARU_DEBYE = 2.541746451147111;        // (Coulomb.meter)

// pulse constants
const double GAUSSIAN_CONST = 2 * std::log(2);
const double SECH_CONST = std::log(3 + 2 * std::sqrt(2));

const double STATE_XX = 0;
const double STATE_YY = 1;
const double STATE_BB = 2;
const double STATE_OO = 3;

const std::string DOT1 = "DOT1";
const std::string DOT2 = "DOT2";
const std::string DOT3 = "DOT3";

// string constants used to convert between units (convert.h)
const std::string FEMTO_TO_ARU = "FEMTO_TO_ARU";
const std::string ARU_TO_FEMTO = "ARU_TO_FEMTO";
const std::string FEMTO2_TO_ARU = "FEMTO2_TO_ARU";
const std::string ARU_TO_FEMTO2 = "ARU_TO_FEMTO2";
const std::string FEMTO3_TO_ARU = "FEMTO3_TO_ARU";
const std::string ARU_TO_FEMTO3 = "ARU_TO_FEMTO3";
const std::string FEMTO4_TO_ARU = "FEMTO4_TO_ARU";
const std::string ARU_TO_FEMTO4 = "ARU_TO_FEMTO4";
const std::string PICO_TO_ARU = "PICO_TO_ARU";
const std::string ARU_TO_PICO = "ARU_TO_PICO";
const std::string PICO2_TO_ARU = "PICO2_TO_ARU";
const std::string INV_PICO_TO_ARU = "INV_PICO_TO_ARU";
const std::string ARU_TO_INV_PICO = "ARU_TO_INV_PICO";
const std::string ELEC_TO_ARU = "ELEC_TO_ARU";
const std::string ARU_TO_ELEC = "ARU_TO_ELEC";
const std::string DEBYE_TO_ARU = "DEBYE_TO_ARU";
const std::string ARU_TO_DEBYE = "ARU_TO_DEBYE";
const std::string EV_TO_ARU = "EV_TO_ARU";
const std::string ARU_TO_EV = "ARU_TO_EV";
const std::string AREA_TO_ELEC = "AREA_TO_ELEC";
const std::string AREA_TO_ELECARU = "AREA_TO_ELECARU";
const std::string ARU_TO_FBWIDTH = "ARU_TO_FBWIDTH";
const std::string EO_TO_AREA = "EO_TO_AREA";
const std::string FRAC_TO_ANGLE = "FRAC_TO_ANGLE";
const std::string ANGLE_TO_FRAC = "ANGLE_TO_FRAC";
const std::string AREA_TO_ARU = "AREA_TO_ARU";
const std::string ARU_TO_AREA = "ARU_TO_AREA";
const std::string NONE = "NONE";

const std::string TO_ARU = "TO_ARU";
const std::string FROM_ARU = "FROM_ARU";

const std::string GAUSSIAN = "GAUSSIAN";
const std::string SECH = "SECH";
const std::string SQUARE = "SQUARE";
const std::string LORENTZIAN = "LORENTZIAN";
const std::string DICHROMATIC = "DICHROMATIC";

const std::string PUMP = "PUMP";
const std::string PROBE = "PROBE";

const std::string AMPMASK_CHEN = "AMPMASK_CHEN";
const std::string PHASEMASK_COS = "PHASEMASK_COS";
const std::string PHASEMASK_CHIRP = "PHASEMASK_CHIRP";
const std::string UNSHAPED = "UNSHAPED";

const std::string ARP = "ARP";
const std::string CROT = "CROT";
const std::string TLS = "TLS";
const std::string THREETLS = "3TLS";

const std::string OPT = "OPT";
const std::string TDEP = "TDEP";
const std::string EXP = "EXP";
const std::string DTRANS = "DTRANS";

const std::string NONLINEAR = "NONLINEAR";
const std::string GENETIC = "GENETIC";

const std::string POL_H = "POL_H";
const std::string POL_V = "POL_V";
const std::string POL_DIA = "POL_DIA";
const std::string POL_ADIA = "POL_ADIA";
const std::string POL_LCP = "POL_LCP";
const std::string POL_RCP = "POL_RCP";
const std::string POL_LIN = "POL_LIN";