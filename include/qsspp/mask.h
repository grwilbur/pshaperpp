#pragma once

#include <cmath>
#include <complex>
#include <functional>
#include <iostream>
#include <stdexcept>
#include <vector>
#include <map>

// Namespace for all mask realted classes and functions.
namespace mask {

/*-----------------------------------------------------------------------*/
// Defines a mask.
// Each mask has a phase and amplitude components.

// The phase components are stored in 4 variables which are used to
// parameterize the cosine phase mask.

// Because amplitude masks are arbitrary, the amplitude components
// are stored as both a function and a vector parameters which
// parameterize that function. All amp mask functions have
// the following signature:

class mask {
 public:
  virtual std::complex<double> evaluate(double) const noexcept = 0;
  virtual ~mask() = default;
};

class cosine : public mask {
  // Cosine phase mask:
  // M(w) = e^(i a * cos(g * (w - w_0) - d))
 public:
  double alpha{0},
         gamma{0},
         omega_0{0},
         delta{0};
  
  std::complex<double> evaluate(double) const noexcept override;

  cosine() = default;
  cosine(double, double, double, double);
};

class bichromatic : public mask {
  // Bichromatic amplitude mask:
  // M(w) = 1-e^-width(omega - omega_0)^2
 public:
  double hole_full_width_half_max{0};
  double omega_0{0};

  double width_param = 2.77258872223978123766892848583270627230200053744102 / 
                      (hole_full_width_half_max * hole_full_width_half_max);

  std::complex<double> evaluate(double) const noexcept override;

  bichromatic() = default;
  bichromatic(double, double);
};

class none : public mask {
 public:
  std::complex<double> evaluate(double) const noexcept override;
  none() = default;
};





}  // namespace mask
